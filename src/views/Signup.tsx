import { Button, Container, Grid} from "@mui/material";
import React, { useRef, useState} from "react";
import { Form } from "react-bootstrap";
import { auth } from "../services/firebase";
import DataService from "../services/service";

type SignUpProps = {
    onClick: () => void;
};

const SignUp: React.FC<SignUpProps> = (props) => {
const { onClick } = props;

const initialUserState = {
  FirstName: "Kevin",
  LastName: "Minionsson",
  PhoneNumber: 111,
  StreetAddress: "Underground",
  Postalcode: 0,
  DateOfBirth: 0,
  PersonalCivilNumber: 0,
  ClearingNumber: 0,
  BankAccountNumber: 0,
}

const emailRef = useRef<HTMLInputElement>(null);
const passwordRef = useRef<HTMLInputElement>(null);

const [user, setUser] = useState(initialUserState);

const [submitted, setSubmitted] = useState(false);

const handleInputChange = (event: any) => {
  const { name, value } = event.target;
  setUser({ ...user, [name]: value });
};

const createAccount = async () => {

  var data = {
    FirstName: user.FirstName,
    LastName: user.LastName,
    PhoneNumber: user.PhoneNumber,
    StreetAddress: user.StreetAddress,
    Postalcode: user.Postalcode,
    DateOfBirth: user.DateOfBirth,
    PersonalCivilNumber: user.PersonalCivilNumber,
    ClearingNumber: user.ClearingNumber,
    BankAccountNumber: user.BankAccountNumber,
  }
  
  DataService.create(data)
  .then(() => {
    setSubmitted(true);
  })
  .catch(e => {
    console.log(e);
  });

  try {
    await auth.createUserWithEmailAndPassword(
      emailRef.current!.value,
      passwordRef.current!.value
    );
  } catch (error) {
    console.error(error);
  } 
   try {
    await auth.signInWithEmailAndPassword(
      emailRef.current!.value,
      passwordRef.current!.value
    );
  } catch (error) {
    console.error(error);
  }
};

  return (
  <Container>
<Grid container spacing={1} style={{marginTop: 4}}>
<Grid item xs={7} >
<Form.Group controlId="formEmail">
      <Form.Label>Email</Form.Label>
      <Form.Control ref={emailRef} type="email" placeholder="email" />
    </Form.Group>
    </Grid>
    <Grid item xs={7} >
    <Form.Group controlId="formPassword">
      <Form.Label>Password</Form.Label>
      <Form.Control
        ref={passwordRef}
        type="password"
        placeholder="password"
      />
    </Form.Group>
    </Grid>
<Grid item xs={7} >

      <div className="form-group">
            <label htmlFor="FirstName">First name</label>
            <input
              type="text"
              className="form-control"
              id="FirstName"
              required
              value={user.FirstName}
              onChange={handleInputChange}
              name="FirstName"
            />
          </div>
          <div className="form-group">
            <label htmlFor="LastName">Last Name</label>
            <input
              type="text"
              className="form-control"
              id="LastName"
              required
              value={user.LastName}
              onChange={handleInputChange}
              name="LastName"
            />
                      </div>
          <div className="form-group">
            <label htmlFor="PhoneNumber">Phone Number</label>
            <input
              type="text"
              className="form-control"
              id="PhoneNumber"
              required
              value={user.PhoneNumber}
              onChange={handleInputChange}
              name="PhoneNumber"
            />
            </div>
          <div className="form-group">
            <label htmlFor="StreetAddress">Street Address</label>
            <input
              type="text"
              className="form-control"
              id="StreetAddress"
              required
              value={user.StreetAddress}
              onChange={handleInputChange}
              name="StreetAddress"
            />
                      </div>
          <div className="form-group">
            <label htmlFor="Postalcode">Postal code</label>
            <input
              type="text"
              className="form-control"
              id="Postalcode"
              required
              value={user.Postalcode}
              onChange={handleInputChange}
              name="Postalcode"
            />
                      </div>
          <div className="form-group">
            <label htmlFor="DateOfBirth">Date Of Birth</label>
            <input
              type="text"
              className="form-control"
              id="DateOfBirth"
              required
              value={user.DateOfBirth}
              onChange={handleInputChange}
              name="DateOfBirth"
            />
          </div>
          <div className="form-group">
            <label htmlFor="PersonalCivilNumber">Personal Civil Number</label>
            <input
              type="number"
              className="form-control"
              id="PersonalCivilNumber"
              required
              value={user.PersonalCivilNumber}
              onChange={handleInputChange}
              name="PersonalCivilNumber"
            />
            </div>
          <div className="form-group">
            <label htmlFor="ClearingNumber">Clearing Number</label>
            <input
              type="text"
              className="form-control"
              id="ClearingNumber"
              required
              value={user.ClearingNumber}
              onChange={handleInputChange}
              name="ClearingNumber"
            />
            </div>
          <div className="form-group">
            <label htmlFor="BankAccountNumber">Bank Account Number</label>
            <input
              type="text"
              className="form-control"
              id="BankAccountNumber"
              required
              value={user.BankAccountNumber}
              onChange={handleInputChange}
              name="BankAccountNumber"
            />
          </div>

        <Button onClick={createAccount} type="button" variant="contained">
          Sign Up
        </Button>
        <Button onClick={() => onClick()} type="button" variant="contained">
          Go Back
        </Button>
      </Grid>
      </Grid>
</Container>
 )
};

export default SignUp;


