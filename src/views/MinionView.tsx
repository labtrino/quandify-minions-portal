import { Button, Container } from "@mui/material";
import React, { useContext } from "react";
import minion from "../minion.jpg";
import { AuthContext } from "../context/AuthContext";
import  { useState } from 'react';




const MinionView: React.FC = () => {

const user = useContext(AuthContext);

const [clicked, setClicked] = useState(true);
const [clicked2, setClicked2] = useState(true);

  return (<Container>
  <h2 className="mt-4 text-center">Welcome {user?.email}</h2>
   <img src={minion} alt="hej" height={500} style={{alignSelf: "center"}} />
    <Button
      sx={{ 
      color: 'orange', 
      backgroundColor: clicked ? 'yellow' : 'red', 
      border: '1px solid green' }}
      onClick={ () => {setClicked(!clicked); alert('You have applied to this timeslot!');} }
    >
  Wednesday 11:00 - 17:00
    </Button>
    <Button
      sx={{ 
      color: 'orange', 
      backgroundColor: clicked2 ? 'yellow' : 'red', 
      border: '1px solid green' }}
      onClick={ () => {setClicked2(!clicked2); alert('You have applied to this timeslot!');} }
    >
  Thursday 11:00 - 17:00
    </Button>
    </Container>
 )
};

export default MinionView;
