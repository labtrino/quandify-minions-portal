import { Button, Container, Grid } from "@mui/material";
import React, { useRef } from "react";
import { Form } from "react-bootstrap";
import { auth } from "../services/firebase";

type SignInProps = {
    onClick: () => void;
};

const SignIn: React.FC<SignInProps> = (props) => {

    const { onClick } = props;

    const emailRef = useRef<HTMLInputElement>(null);
    const passwordRef = useRef<HTMLInputElement>(null);

    const signIn = async () => {
        try {
          await auth.signInWithEmailAndPassword(
            emailRef.current!.value,
            passwordRef.current!.value
          );
        } catch (error) {
          console.error(error);
        }
      };
    

  return (
  <Container>
    <Grid container spacing={1}>
      <Grid item xs={7}>
    <Form.Group controlId="formEmail">
      <Form.Label>Email</Form.Label>
      <Form.Control ref={emailRef} type="email" placeholder="email" />
    </Form.Group>
    </Grid>
    <Grid item xs={7}>
    <Form.Group controlId="formPassword">
      <Form.Label>Password</Form.Label>
      <Form.Control
        ref={passwordRef}
        type="password"
        placeholder="password"
      />
    </Form.Group>
      </Grid>

      <Grid item xs={6}>
        <Button onClick={() => onClick()} type="button" variant="contained">
          Sign Up
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Button
          onClick={signIn}
          type="button"
          variant="contained"
          >
          Sign In
        </Button>

          </Grid>
          </Grid>
</Container>
 
 )


};

export default SignIn;
