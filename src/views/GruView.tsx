import { Button, Container } from "@mui/material";
import React from "react";
import gru from "../gru.jpeg";


const GruView: React.FC = () => {

  return (<Container>
  <h2 className="mt-4 text-center">Welcome Gru(Admin)</h2>
   <img src={gru} alt="hej" height={500} style={{alignSelf: "center"}} />
    <Button
      sx={{ 
      color: 'orange', 
      backgroundColor: 'grey', 
      border: '1px solid green' }}
      disabled 
    >
  Wednesday 11:00 - 17:00 Astrid!
    </Button>
    </Container>
 )
};

export default GruView;

