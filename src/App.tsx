import { useContext, useState } from "react";
import { Navbar } from "react-bootstrap";
import { AuthContext } from "./context/AuthContext";
import { auth } from "./services/firebase";
import { Button } from "@mui/material";
import MinionView from "./views/MinionView";
import GruView from "./views/GruView";
import SignIn from "./views/SignIn";
import SignUp from "./views/Signup";

function App() {
  const user = useContext(AuthContext);

  const [clicked, setClicked] = useState(false);

  const signOut = async () => {
    await auth.signOut();
  };

  return (
    <>
      <Navbar className="justify-content-between" bg="dark" variant="dark" >
        <Navbar.Brand style={{marginLeft: 40}}>Quandify Portal</Navbar.Brand>
        {user && <Button onClick={signOut}>Sign Out</Button>}
      </Navbar>
      {!user ? 
      ( !clicked ? 
        <SignIn onClick={() => setClicked(true)} /> 
        : 
        <SignUp onClick={() => setClicked(false)}/>
      )
       : user.email === "admin@quandifyminions.com" ? <GruView /> : <MinionView />
      }
    </>
  );
}

export default App;

