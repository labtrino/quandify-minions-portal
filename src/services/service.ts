import firebase from "./firebase";


export interface UserProps {
    FirstName: string,
    LastName: string,
    PhoneNumber: number,
    StreetAddress: string,
    Postalcode: number,
    DateOfBirth: number,
    PersonalCivilNumber: number,
    ClearingNumber: number,
    BankAccountNumber: number;
  }

const db = firebase.collection("/users");

const getAll = () => {
  return db;
};

const create = (data: UserProps) => {
  return db.add(data);
};

const update = (id: string, value: any) => {
  return db.doc(id).update(value);
};

const remove = (id: string) => {
  return db.doc(id).delete();
};

const DataService = {
  getAll,
  create,
  update,
  remove
}; 

export default DataService;