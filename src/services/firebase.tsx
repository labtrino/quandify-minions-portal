import firebase from "firebase/compat/app";
import "firebase/compat/auth";
import "firebase/compat/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAmZ0UfcDaKht_4YYbZ4T7jM4afaP1knqE",
  authDomain: "quandify-minions.firebaseapp.com",
  projectId: "quandify-minions",
  storageBucket: "quandify-minions.appspot.com",
  messagingSenderId: "712673359455",
  appId: "1:712673359455:web:fc91fb7cc568de9cd7cdea"
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export default firebase.firestore();
